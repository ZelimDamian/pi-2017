import {
  IonButtons, IonCard, IonCardContent, IonCardHeader,
  IonContent,
  IonHeader, IonImg, IonItem, IonLabel, IonList,
  IonMenuButton,
  IonPage,
  IonSegment, IonSegmentButton,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import React from 'react';
import { useParams } from 'react-router';
import './Orders.css';

import {orders} from '../../data/orders.json'

const Orders: React.FC = () => {

  const { name } = useParams<{ name: string; }>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Заказы</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>

        <IonSegment className="active-toggle" onIonChange={e => console.log('Segment selected', e.detail.value)}>
          <IonSegmentButton value="active">
            <IonLabel>Активные</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="completed">
            <IonLabel>Завершенные</IonLabel>
          </IonSegmentButton>
        </IonSegment>


        {orders.map(order => (

          <IonCard>
            <IonCardHeader>
              <IonImg src="https://static-maps.yandex.ru/1.x/?ll=37.620070,55.753630&size=450,230&z=13&l=map&pt=37.620070,55.753630,pmwtm1~37.64,55.76363,pmwtm99
  " />
            </IonCardHeader>

            <IonCardContent>
              <IonList>
                <IonItem>
                  {order.date} {order.price}
                </IonItem>
                <IonItem>
                  {order.category.name}, {order.vehicle_type.name}
                </IonItem>
              </IonList>
            </IonCardContent>
          </IonCard>
        ))}

      </IonContent>
    </IonPage>
  );
};

export default Orders;
