from rest_framework import viewsets
from drivers.models import Driver, VehicleImage, VehicleBrand, VehicleType, Vehicle, VehicleModel
from drivers.serializers import DriverSerializer, VehicleBrandSerializer, VehicleImageSerializer,\
    VehicleModelSerializer, VehicleSerializer,  VehicleTypeSerializer


class DriversViewSet(viewsets.ModelViewSet):
    queryset = Driver.objects.all().order_by('-created_at')
    serializer_class = DriverSerializer


class VehicleBrandViewSet(viewsets.ModelViewSet):
    queryset = VehicleBrand.objects.all().order_by('-created_at')
    serializer_class = VehicleBrandSerializer


class VehicleImageViewSet(viewsets.ModelViewSet):
    queryset = VehicleImage.objects.all().order_by('-created_at')
    serializer_class = VehicleImageSerializer


class VehicleModelViewSet(viewsets.ModelViewSet):
    queryset = VehicleModel.objects.all().order_by('-created_at')
    serializer_class = VehicleModelSerializer


class VehicleViewSet(viewsets.ModelViewSet):
    queryset = Vehicle.objects.all().order_by('-created_at')
    serializer_class = VehicleSerializer


class VehicleTypeViewSet(viewsets.ModelViewSet):
    queryset = VehicleType.objects.all().order_by('-created_at')
    serializer_class = VehicleTypeSerializer
