from rest_framework import serializers

from .models import Driver, VehicleBrand, VehicleType, VehicleModel, Vehicle, VehicleImage, VehicleStatus


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = '_all_'


class VehicleBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleBrand
        fields = '_all_'


class VehicleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleType
        fields = '_all_'


class VehicleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleModel
        fields = '_all_'


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '_all_'


class VehicleImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleImage
        fields = '_all_'


class VehicleStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleStatus
        fields = '_all_'