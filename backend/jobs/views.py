from rest_framework import viewsets

from .models import Job, JobImage, Category, CategoryVehicleType, ReviewImage, Order, OrderStatus, \
    OrderCancellation, JobStatus, Offer, Review, UserReason, DriverReason

from jobs.serializers import JobSerializer, JobImageSerializer, CategorySerializer, CategoryVehicleTypeSerializer, \
    ReviewImageSerializer, OrderSerializer, OrderStatusSerializer, OrderCancellationSerializer, JobStatusSerializer, \
    OfferSerializer, ReviewSerializer, UserReasonSerializer, DriverReasonSerializer


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all().order_by('-created_at')
    serializer_class = JobSerializer


class JobImageViewSet(viewsets.ModelViewSet):
    queryset = JobImage.objects.all().order_by('-created_at')
    serializer_class = JobImageSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('-created_at')
    serializer_class = CategorySerializer


class CategoryVehicleTypeViewSet(viewsets.ModelViewSet):
    queryset = CategoryVehicleType.objects.all().order_by('-created_at')
    serializer_class = CategoryVehicleTypeSerializer


class ReviewImageViewSet(viewsets.ModelViewSet):
    queryset = ReviewImage.objects.all().order_by('-created_at')
    serializer_class = ReviewImageSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by('-created_at')
    serializer_class = OrderSerializer


class OrderStatusViewSet(viewsets.ModelViewSet):
    queryset = OrderStatus.objects.all()
    serializer_class = OrderStatusSerializer


class OrderCancellationViewSet(viewsets.ModelViewSet):
    queryset = OrderCancellation.objects.all().order_by('-created_at')
    serializer_class = OrderCancellationSerializer


class JobStatusViewSet(viewsets.ModelViewSet):
    queryset = JobStatus.objects.all()
    serializer_class = JobStatusSerializer


class OfferViewSet(viewsets.ModelViewSet):
    queryset = Offer.objects.all().order_by('-created_at')
    serializer_class = OfferSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all().order_by('-created_at')
    serializer_class = ReviewSerializer


class UserReasonViewSet(viewsets.ModelViewSet):
    queryset = UserReason.objects.all().order_by('-created_at')
    serializer_class = UserReasonSerializer


class ViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all().order_by('-created_at')
    serializer_class = JobSerializer


class DriverReasonViewSet(viewsets.ModelViewSet):
    queryset = DriverReason.objects.all()
    serializer_class = DriverReasonSerializer
