from rest_framework import serializers

from .models import Job, JobImage, Category, CategoryVehicleType,  ReviewImage, Order, OrderStatus, \
    OrderCancellation, JobStatus, Offer, Review, UserReason, DriverReason


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '_all_'


class JobImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobImage
        fields = '_all_'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '_all_'


class CategoryVehicleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryVehicleType
        fields = '_all_'


class OfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = '_all_'


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '_all_'


class ReviewImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewImage
        fields = '_all_'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '_all_'


class OrderCancellationSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderCancellation
        fields = '_all_'


class JobStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobStatus
        fields = '_all_'


class OrderStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderStatus
        fields = '_all_'


class UserReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserReason
        fields = '_all_'


class DriverReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverReason
        fields = '_all_'
