from core.models import *
from drivers.models import *
from enum import Enum


class JobStatus(Enum):
    draft = 0
    published = 1
    archived = 2

    @classmethod
    def values(cls):
        return [(attr.value, attr.name) for attr in cls]


class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True)
    unit = models.CharField(max_length=255)
    unit_price = models.CharField(max_length=255)
    parent = models.ForeignKey('Category', blank=True, null=True, on_delete=models.DO_NOTHING)
    image = models.ForeignKey(Image, blank=True, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class Job(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    description = models.CharField(max_length=255)
    address = models.ForeignKey(Address, on_delete=models.DO_NOTHING)
    dropoff = models.ForeignKey(Address, related_name='dropoff_job_set', blank=True, on_delete=models.DO_NOTHING)
    quantity = models.DecimalField(decimal_places=5, max_digits=20)
    date = models.DateTimeField()
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.DO_NOTHING)
    status = models.IntegerField(choices=JobStatus.values())
    customer = models.ForeignKey(core.models.User, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class CategoryVehicleType(models.Model):
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class JobImage(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    image = models.ForeignKey(core.models.Image, blank=True, null=True, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class Offer(models.Model):
    job = models.ForeignKey(Job, on_delete=models.DO_NOTHING)
    amount = models.DecimalField(decimal_places=5, max_digits=20)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class Review(models.Model):
    order = models.ForeignKey()
    value = models.IntegerField()
    message = models.ForeignKey(Message, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class ReviewImage(models.Model):
    review = models.ForeignKey(Review, on_delete=models.DO_NOTHING)
    image = models.ForeignKey(Image, blank=True, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class OrderStatus(Enum):
    accepted = 0
    inProgress = 1
    completed = 2
    cancelled = 3

    @classmethod
    def values(cls):
        return [(attr.value, attr.name) for attr in cls]


class Order(models.Model):
    offer = models.ForeignKey(Offer, on_delete=models.DO_NOTHING)
    status = models.IntegerField(choices=OrderStatus.values())
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')


class UserReason(Enum):
    do_not_need_anymore = 0
    driver_asked_to_cancel = 1

    @classmethod
    def values(cls):
        return [(attr.value, attr.name) for attr in cls]


class DriverReason(Enum):
    unable_to_fulfill = 0
    vehicle_issue = 1

    @classmethod
    def values(cls):
        return [(attr.value, attr.name) for attr in cls]


class OrderCancellation(models.Model):
    order = models.ForeignKey(Order, on_delete=models.DO_NOTHING)
    user_reason = models.IntegerField(choices=UserReason.values())
    driver_reason = models.IntegerField(choices=DriverReason.values())
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
